import { Injectable,Inject } from '@nestjs/common';
import { Model } from 'mongoose';
import { IUser } from './interfaces/user.interface';
import { CreateUserDto } from './dto/create-user.dto';
import { IUsersService } from './interfaces/iusers.service';

@Injectable()
export class UsersService implements IUsersService {
    constructor(@Inject('USER_MODEL') private readonly userModel: Model<IUser>) {}

    async create(createUserDto: CreateUserDto): Promise<IUser> {
        console.log('Calling users service create method');

        const createdUser = new this.userModel(createUserDto);
        return await createdUser.save();
      }

    async findAll(): Promise<IUser[]> {
        console.log('Calling users service findAll method');

        return await this.userModel.find().exec();
    }

    async findById(ID: number): Promise<IUser | null> {
        console.log('Calling users service findById method');

        return;
    }

   async findOne(options: object): Promise<IUser | null> {
        console.log('Calling users service findOne method');

        return;
    }

    async update(ID: number, newValue: IUser): Promise<IUser | null> {
        console.log('Calling users service update method');

        return;
    }

    async delete(ID: number): Promise<string> {
        console.log('Calling users service delete method');

        return;
    }
}
